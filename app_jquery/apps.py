from django.apps import AppConfig


class AppJqueryConfig(AppConfig):
    name = 'app_jquery'
