from django.test import TestCase, Client, LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
import pytz
import datetime
from django.urls import resolve
from ppw_story6.settings import BASE_DIR
import os

# Create your tests here.

class TestApp_Login(LiveServerTestCase):

    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=options)
        #self.browser = webdriver.Chrome(chrome_options=options)
        self.client = Client()

        #Creating Account
        self.browser.get('%s%s' % (self.live_server_url, '/story9/signup/'))
        user = self.browser.find_element_by_id('id_username')
        pswd = self.browser.find_element_by_id('id_password1')
        pswd_con = self.browser.find_element_by_id('id_password2')
        signup = self.browser.find_element_by_id('but-sign')
        user.send_keys("Gagah")
        pswd.send_keys("BambangJantan2314")
        pswd_con.send_keys("BambangJantan2314")
        signup.click()
        



    def tearDown(self):
        self.browser.quit()

    def test_login_exist(self):
        client = self.client
        response = client.get("/story9/login/")
        self.assertEqual(200, response.status_code)
    
    def test_signup_exist(self):
        client = self.client
        response = client.get("/story9/signup/")
        self.assertEqual(200, response.status_code)
    
    def test_page_notexists(self):
        client = self.client
        response = client.get("/story9/lxjabs")
        self.assertEqual(404, response.status_code)
    
    def test_correct_html(self):
        response = self.client.get('/story9/login/')
        self.assertTemplateUsed(response, 'app_login/login.html')

    
    def test_check_home_not_login(self):
        browser = self.browser
        # Opening the link we want to test
        #browser.get('http://127.0.0.1:8000/')
        browser.get('%s%s' % (self.live_server_url, '/story9/'))
        # Test if redirect to login
        self.assertIn("Login</button>", browser.page_source)
    
    def test_check_home_login_and_logout(self):
        browser = self.browser
        # Opening the link we want to test
        #browser.get('http://127.0.0.1:8000/')
        browser.get('%s%s' % (self.live_server_url, '/story9/login/'))

        #login
        user = self.browser.find_element_by_id('id_username')
        pswd = self.browser.find_element_by_id('id_password')
        login = self.browser.find_element_by_id('but-log')
        user.send_keys("Gagah")
        pswd.send_keys("BambangJantan2314")
        login.click()

        self.assertIn("Welcome, Gagah", browser.page_source)

        logout = self.browser.find_element_by_id('but-out')
        logout.click()
    
    def test_if_login_access_login_or_signup_redirect(self):
        browser = self.browser
        # Opening the link we want to test
        #browser.get('http://127.0.0.1:8000/')
        browser.get('%s%s' % (self.live_server_url, '/story9/login/'))

        #login
        user = self.browser.find_element_by_id('id_username')
        pswd = self.browser.find_element_by_id('id_password')
        login = self.browser.find_element_by_id('but-log')
        user.send_keys("Gagah")
        pswd.send_keys("BambangJantan2314")
        login.click()


        browser.get('%s%s' % (self.live_server_url, '/story9/login/'))
        self.assertIn("Welcome, Gagah", browser.page_source)

        browser.get('%s%s' % (self.live_server_url, '/story9/signup/'))
        self.assertIn("Welcome, Gagah", browser.page_source)

        logout = self.browser.find_element_by_id('but-out')
        logout.click()
    
    def test_success_signup(self):
        browser = self.browser
        browser.get('%s%s' % (self.live_server_url, '/story9/signup/'))
        user = browser.find_element_by_id('id_username')
        pswd = browser.find_element_by_id('id_password1')
        pswd_con = browser.find_element_by_id('id_password2')
        signup = browser.find_element_by_id('but-sign')
        user.send_keys("Gagah2")
        pswd.send_keys("BambangJantan2314")
        pswd_con.send_keys("BambangJantan2314")
        signup.click()
        self.assertIn("Login</button>", browser.page_source)

    
    def test_failed_signup(self):
        browser = self.browser
        browser.get('%s%s' % (self.live_server_url, '/story9/signup/'))
        user = browser.find_element_by_id('id_username')
        pswd = browser.find_element_by_id('id_password1')
        pswd_con = browser.find_element_by_id('id_password2')
        signup = browser.find_element_by_id('but-sign')
        user.send_keys("&*(&^@*&^%^)")
        pswd.send_keys("2314")
        pswd_con.send_keys("2314")
        signup.click()
        self.assertIn("Sign Up</button>", browser.page_source)
    
    def test_failed_login(self):
        browser = self.browser
        browser.get('%s%s' % (self.live_server_url, '/story9/login/'))
        user = browser.find_element_by_id('id_username')
        pswd = browser.find_element_by_id('id_password')
        
        login = browser.find_element_by_id('but-log')
        user.send_keys("&*(&^@*&^%^)")
        pswd.send_keys("2314")
        
        login.click()
        self.assertIn("Login</button>", browser.page_source)



        

        
    

    

    
    
    
