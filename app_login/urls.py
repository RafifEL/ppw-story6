from django.urls import path
from . import views


app_name = 'app_login'

urlpatterns = [
    path('login/', views.login_view, name = 'login'),
    path('', views.home, name = 'home' ),
    path('logout/', views.logout_view, name = 'logout'),
    path('signup/', views.signUp_view, name = 'signup' )
]