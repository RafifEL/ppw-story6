from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required


# Create your views here.
def home(request):
    if request.user.is_authenticated:
        return render(request, "app_login/home.html")
    return redirect('/story9/login/')

def signUp_view(request):
    if request.user.is_authenticated:
        return redirect('/story9/')

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/story9/login/')
    else:
        form = UserCreationForm()
    return render(request, 'app_login/signup.html', {'form': form})


def login_view(request):
    if request.user.is_authenticated:
        return redirect('/story9/')

    if request.method == "POST":
        print(request.POST)
        form = AuthenticationForm(data = request.POST)
        
        if form.is_valid():
            user = form.get_user()
            print(user)
            login(request, user)
            return redirect('/story9/')
    else:
        form = AuthenticationForm()
    return render(request, "app_login/login.html", {"form" : form })

def logout_view(request):
    logout(request)
    return redirect("/story9/login")
        


