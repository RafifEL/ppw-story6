$(document).ready(() =>{
    $("#result").hide();
    $("#failed").hide();
    $.ajax({
        method: 'GET',
        url: '/story8/search?key=Data',
        success: function(response){
            var contents1 = response.items
            if (contents1 != undefined){
                $("#content").empty();
                $("#result").show();
                for(var i = 0; i < contents1.length; i++){
                    var row = document.createElement("tr");

                    var numRow = document.createElement("th")
                    numRow.scope = "row"
                    numRow.innerHTML = i + 1;
                    $(row).append(numRow);

                    var name = document.createElement("td");
                    name.innerHTML = contents1[i].volumeInfo.title;
                    $(row).append(name);

                    var thumbnail = document.createElement("td");
                    try{
                        var img = document.createElement("img");
                        img.src = contents1[i].volumeInfo.imageLinks.thumbnail;
                        img.alt = "NotFound"
                        $(thumbnail).append(img);
                    }
                    catch{
                        $(thumbnail).append("Picture not Found");

                    }
                    
                    $(row).append(thumbnail);


                    var authors = document.createElement("td");
                    let list = document.createElement("ul");
                    
                    try{
                        for (let j = 0; j < contents1[i].volumeInfo.authors.length; j++){
                            $(list).append("<li>"+ contents1[i].volumeInfo.authors[j] +"</li>")
                        }  
                    }
                    catch(err){
                        $(list).append("<li> Authors Not Found </li>")
                    }
                    $(authors).append(list)
                    $(row).append(authors)


                    var detail = document.createElement("td");
                    var link = document.createElement("a")
                    link.href = contents1[i].volumeInfo.previewLink
                    $(link).append(contents1[i].volumeInfo.previewLink ) 
                    $(detail).append(link);
                    $(row).append(detail);

                    $("#content").append(row);

                }
            }
            else{
                // $("#result").hide();
                alert("Your Book is not Found")
        
            }

        }
    })

    $("#submit").click(() => {
        let key = $("#searchBar").val();
        if (key == ""){
            alert("Fill the Search Box")
            throw new Error('No Fill Box');

        }
        $("#searchBar").val("");
        
        $.ajax({
            method: 'GET',
            url: '/story8/search?key=' + key,
            success: function(response){
                console.log(response)
                var contents = response.items
                if (contents != undefined){
                    $("#content").empty();
                    $("#result").show();
                    for(var i = 0; i < contents.length; i++){
                        var row = document.createElement("tr");
    
                        var numRow = document.createElement("th")
                        numRow.scope = "row"
                        numRow.innerHTML = i + 1;
                        $(row).append(numRow);
    
                        var name = document.createElement("td");
                        name.innerHTML = contents[i].volumeInfo.title;
                        $(row).append(name);
    
                        var thumbnail = document.createElement("td");
                        try{
                            var img = document.createElement("img");
                            img.src = contents[i].volumeInfo.imageLinks.thumbnail;
                            img.alt = "NotFound"
                            $(thumbnail).append(img);
                        }
                        catch{
                            $(thumbnail).append("Picture not Found");

                        }
                        
                        $(row).append(thumbnail);
    
    
                        var authors = document.createElement("td");
                        let list = document.createElement("ul");
                        
                        try{
                            for (let j = 0; j < contents[i].volumeInfo.authors.length; j++){
                                $(list).append("<li>"+ contents[i].volumeInfo.authors[j] +"</li>")
                            }  
                        }
                        catch(err){
                            $(list).append("<li> Authors Not Found </li>")
                        }
                        $(authors).append(list)
                        $(row).append(authors)
    
    
                        var detail = document.createElement("td");
                        var link = document.createElement("a")
                        link.href = contents[i].volumeInfo.previewLink
                        $(link).append(contents[i].volumeInfo.previewLink ) 
                        $(detail).append(link);
                        $(row).append(detail);
    
                        $("#content").append(row);

                    }
                }
                else{
                    // $("#result").hide();
                    alert("Your Book is not Found")
            
                }

            }
        })
    })

})