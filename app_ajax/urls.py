from django.urls import path
from . import views


app_name = 'app_ajax'

urlpatterns = [
    path("", views.searchPage, name = 'home'),
    path("search", views.search, name = 'search'),
    
]