from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.
def searchPage(request):
    return render(request, "app_ajax/base.html")

def search(request):
    key = request.GET['key']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + key
    response = requests.get(url)
    response_json = response.json()

    return JsonResponse(response_json)