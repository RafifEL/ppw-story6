from django.test import TestCase, Client, LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import searchPage, search
import pytz
import datetime
from django.urls import resolve
from ppw_story6.settings import BASE_DIR
import os

# Create your tests here.
class app_jquery_Test(LiveServerTestCase):
#    def setUp(self):
#         options = Options()
#         options.add_argument('--headless')
#         options.add_argument('--no-sandbox') 
#         self.browser = webdriver.Chrome(chrome_options=options)
#         # self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=options)
#         self.client = Client()
    
#     def tearDown(self):
#         self.browser.quit()
    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=options)
        # self.browser = webdriver.Chrome(chrome_options=options)
        self.client = Client()
    def tearDown(self):
        self.browser.quit()
    
    def test_url_no_existent(self):
        response = self.client.get("/story8/dwuoahjd")
        self.assertEquals(404, response.status_code)
    
    def test_url_exist(self):
        response = self.client.get("/story8/")
        self.assertEquals(200, response.status_code)
    
    def test_button(self):
        browser = self.browser
        # Opening the link we want to test
        #browser.get('http://127.0.0.1:8000/')
        browser.get('%s%s' % (self.live_server_url, '/story8/'))
        # find the form element
        search = browser.find_element_by_id('searchBar')
        submit = browser.find_element_by_id('submit')
        # Fill the form with data
        search.send_keys('plisi')        
        submit.click()
    
    def test_search_funct(self):
        response = self.client.get("/story8/search?key=plisi")
        self.assertIn("plisi", response.content.decode("utf8"))
