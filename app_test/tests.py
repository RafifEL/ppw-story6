from django.test import TestCase, Client, LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .models import Status
from .views import home
import pytz
import datetime
from django.urls import resolve
from ppw_story6.settings import BASE_DIR
import os

# Create your tests here.
class app_test_Test(LiveServerTestCase):
    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox') 
        #self.browser = webdriver.Chrome(chrome_options=options)
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=options)
        self.client = Client()

    def tearDown(self):
        self.browser.quit()

    def test_nonexistent_url(self):
        c = self.client
        response = c.get('jdsald')
        self.assertEqual(404, response.status_code )
    
    def test_home_urls_exist(self):
        c = self.client
        response = c.get('')
        self.assertEqual(200, response.status_code )
    
    def test_text_halo_apa_kabar_exist(self):
        c = self.client
        response = c.get('')
        self.assertIn("Halo, apa kabar?", response.content.decode('utf8'))
    
    def test_text_apa_ada_form(self):
        c = self.client
        response = c.get('')
        self.assertIn('<form', response.content.decode('utf8'))
        self.assertIn('Status', response.content.decode('utf8'))
    
    def test_home_pake_views_home(self):
        handler = resolve('/')
        self.assertEqual(handler.func, home )

    
    def test_url_POST_home_redirect(self):
        c = self.client
        response = c.post('',{
            'isi_status': "Sedih"
        })
        self.assertEqual(302, response.status_code)

    def test_str_Status(self):
        status = Status(isi_status = "aku")
        status.save()
        self.assertEqual(str(status),"{},{}".format(status.isi_status, str(status.tanggal)))

    def test_submit_form_masuk_database(self):
        c = self.client
        response = c.post("",{
            'isi_status' : "Aku Senang"
        })
        self.assertEqual(1, len(Status.objects.all()))
    
    def test_selenium(self):
        browser = self.browser
        # Opening the link we want to test
        #browser.get('http://127.0.0.1:8000/')
        browser.get(self.live_server_url)
        # find the form element
        status = browser.find_element_by_id('id_isi_status')
        submit = browser.find_element_by_id('submit')
        # Fill the form with data
        status.send_keys('Test masukin key ke form')        
        submit.click()

    def test_isi_form_tampil(self):
        browser = self.browser
        # Buka link
        #browser.get('http://127.0.0.1:8000/')
        browser.get(self.live_server_url)
        # Cari Form dan tombol
        status = browser.find_element_by_id('id_isi_status')
        submit = browser.find_element_by_id('submit')
        # Isi Form
        status.send_keys('Aku Sedih')        
        submit.click()

        tz = pytz.timezone('Asia/Jakarta')
        time = datetime.datetime.now(tz)
        # time_unaware = isi_Form.tanggal.astimezone(pytz.timezone('Asia/Jakarta'))
        self.assertIn("Aku Sedih", browser.page_source)
        self.assertIn(time.strftime('%a, %d/%m/%Y %H:%M'), browser.page_source)
    
    def test_isi_form_kosong_tidak_tampil(self):
        browser = self.browser
        # Buka link
        #browser.get('http://127.0.0.1:8000/')
        browser.get(self.live_server_url)
        # Cari Form dan tombol
        submit = browser.find_element_by_id('submit')        
        submit.click()
        tz = pytz.timezone('Asia/Jakarta')
        time = datetime.datetime.now(tz)
        self.assertNotIn(time.strftime('%a, %d/%m/%Y %H:%M'), browser.page_source)


    
    
        
    


    
    
        
    


    

    


