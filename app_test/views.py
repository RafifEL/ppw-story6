from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

# Create your views here.
def home(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('app_test:home')
    else:
        form = StatusForm()
        all_status = Status.objects.all()
        return render(request, 'home.html', {'form':form, 'statuses' : all_status})