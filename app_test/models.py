from django.db import models

# Create your models here.
class Status(models.Model):
    isi_status = models.TextField(max_length = 300)
    tanggal = models.DateTimeField(auto_now_add = True)
    
    def __str__(self):
        return "{},{}".format(self.isi_status, self.tanggal)

